using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PlayerSelectController))]
public class PlayerSelectInput : MonoBehaviour
{
    // player select scripts
    [SerializeField]
    PlayerSelectController PlayerSelect;

    private int playerNum;

    // give player a controller script if none exists
    private void Awake() 
    {

        if (PlayerSelect == null)
        {
            PlayerSelect = GetComponent<PlayerSelectController>();
        }
        
        //get player number
        playerNum = PlayerSelect.playerNum;

    }


    // handle inputs for character selection
    private void Update() 
    {
        // join game
        if(playerNum > 1)
        {
            if (Input.GetButtonDown("Join_" + playerNum))
            {
                PlayerSelect.OnJoin();
            }
        }

        if (PlayerSelect.Joined)
        {
            // change character
            if (Input.GetButtonDown("ChangeChar_" + playerNum))
            {
                PlayerSelect.OnChange();
            }
        }

        if (playerNum == 1)
        {
            if (Input.GetButtonDown("StartGame_" + playerNum))
            {
                PlayerSelect.StartGame();
            }
        }

    }
}
