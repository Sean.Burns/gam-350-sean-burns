using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerSelectmanager : MonoBehaviour
{
    // Player Objects
    public GameObject player1;
    public GameObject player2;
    public GameObject player3;

    // UI Objects
    public GameObject Player2JoinMessage;
    public GameObject Player3JoinMessage;

    // variables 
    private int playerCount;

    // Start is called before the first frame update
    void Start()
    {
        playerCount = 1;
        player2.SetActive(false);
        player3.SetActive(false);
        Player2JoinMessage.SetActive(true);
        Player3JoinMessage.SetActive(false);
    }

    public void PlayerJoined()
    {
        playerCount += 1;

        // add player 2
        if (playerCount == 2)
        {
            player2.SetActive(true);
            Player2JoinMessage.SetActive(false);
            Player3JoinMessage.SetActive(true);
        }

        // add player 3
        if (playerCount == 3)
        {
            player3.SetActive(true);
            Player3JoinMessage.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
