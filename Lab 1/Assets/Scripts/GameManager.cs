using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public int numPlayers;

    bool playerInstantiate;

    TextMeshProUGUI winMessage;

    private GameObject PlayerPrefab;

    private void Awake() 
    {
        DontDestroyOnLoad(gameObject);

        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(instance);
            instance = this;
        }

        // if character select set num players = 1
        if (SceneManager.GetActiveScene().buildIndex == 0)
        {
            numPlayers = 1;
        }

        //add player 2 and 3
        playerInstantiate = false;
    }


    private void Update() 
    {
        if (!playerInstantiate)
        {
            PlayerInstantiate();
        }

        if (SceneManager.GetActiveScene().buildIndex == 1)
        {
            CheckPlayerCount();
        }
    }

    // check for num players alive
    void CheckPlayerCount()
    {
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");

        if (players.Length == 1)
        {
            StartCoroutine(GameOver());
        }
    }

    // adds needed players
    public void PlayerInstantiate()
    {
        // if battle add players
        if (SceneManager.GetActiveScene().buildIndex == 1)
        {
            // set gameobject to instantiate
            PlayerPrefab = GameObject.FindGameObjectWithTag("Player");

            // instantiate prefabs and set player number and color
            GameObject player2 = Instantiate(PlayerPrefab, GameObject.FindGameObjectWithTag("P2 Spawn").transform.position, Quaternion.Euler(0, 90, 0));
            player2.GetComponent<PlayerController>().playerNum = 2;
            player2.GetComponent<MeshRenderer>().material.color = Color.blue;

            // check if player 3 if so instantiate
            if (numPlayers == 3)
            {
                GameObject player3 = Instantiate(PlayerPrefab, GameObject.FindGameObjectWithTag("P3 Spawn").transform.position, Quaternion.Euler(0, -90, 0));
                player3.GetComponent<PlayerController>().playerNum = 3;
                player3.GetComponent<MeshRenderer>().material.color = Color.red;
            }


            /*if (numPlayers == 2)
            {
                // get players
                GameObject[] players = GameObject.FindGameObjectsWithTag("Player");

                int playerIdx = 0;

                // loop and find player 3
                for (int i = 0; i < players.Length; i++)
                {
                    if (players[i].GetComponent<PlayerController>().playerNum == 3)
                    {
                        playerIdx = i;
                        break;
                    }
                }
                Destroy(players[playerIdx]);
            }*/

            playerInstantiate = true;
        }
    }

    // display win message and load character select
    IEnumerator GameOver()
    {
        // get player who won
        PlayerController winner = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();

        // display message
        winMessage = GameObject.FindGameObjectWithTag("UIMessage").GetComponent<TextMeshProUGUI>();
        winMessage.SetText("Player " + winner.playerNum + " wins!");

        // wait
        yield return new WaitForSeconds(5);

        // load character select
        SceneManager.LoadScene(0);
    }
    
}
