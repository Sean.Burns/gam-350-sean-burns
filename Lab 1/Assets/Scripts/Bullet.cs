using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    //direction 1=up 2=right 3=down 4=left
    public int direction;
    
    // Update is called once per frame
    void Update()
    {
        if (direction == 1)
        {
            gameObject.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 100);
        }
        else if (direction == 2)
        {
            gameObject.GetComponent<Rigidbody>().velocity = new Vector3(-100, 0, 0);
        }
        else if (direction == 3)
        {
            gameObject.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, -100);
        }
        else if (direction == 4)
        {
            gameObject.GetComponent<Rigidbody>().velocity = new Vector3(100, 0, 0);
        }
    }

    // detsroy gameobject and push player
    private void OnCollisionEnter(Collision other) 
    {
        if (other.gameObject.tag == "wall")
        {
            Destroy(gameObject);
        }   
        else if (other.gameObject.tag == "Player")
        {
            // hit player up
            if (direction == 1)
            {
                other.gameObject.GetComponent<Rigidbody>().AddForce(new Vector3(0, 0, -10));
            }
            else if (direction == 2) //hit player fowards
            {
                other.gameObject.GetComponent<Rigidbody>().AddForce(new Vector3(-10, 0, 0));
            }
            else if (direction == 3) // hit player downwards
            {
                other.gameObject.GetComponent<Rigidbody>().AddForce(new Vector3(0, 0, 10));
            }
            else if (direction == 4) // hit player backwards
            {
                other.gameObject.GetComponent<Rigidbody>().AddForce(new Vector3(10, 0, 0));
            }

            Destroy(gameObject);
        }
    }
}
