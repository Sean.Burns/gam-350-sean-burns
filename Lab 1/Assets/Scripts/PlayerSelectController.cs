using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;
using UnityEngine.SceneManagement;

public class PlayerSelectController : MonoBehaviour
{

    // player number
    public int playerNum = 1;
    public bool Joined = true;

    // controller
    private CharacterController controller;

    // list of character types
    [SerializeField]
    private List<GameObject> characterTypes;

    //index
    public int charIndex;
    
    // player object
    [SerializeField]
    GameObject PlayerParent;

    // join ui
    [SerializeField] GameObject joinMessage;

    // game manager
    [SerializeField] GameManager gameManager;

    // set controller
    void Start()
    {
        controller = gameObject.GetComponent<CharacterController>();
        charIndex = 0;
        characterTypes[0].SetActive(true);
        characterTypes[1].SetActive(false);

        if (playerNum > 1)
        {
            Joined = false;
        }
    }

    // change character
    public void OnChange()
    {
        if (charIndex == 0)
        {
            charIndex = 1;
            characterTypes[0].SetActive(false);
            characterTypes[1].SetActive(true);
        }
        else if (charIndex == 1)
        {
            charIndex = 0;
            characterTypes[0].SetActive(true);
            characterTypes[1].SetActive(false);
        }
    }

    // join game 
    public void OnJoin()
    {
        if (playerNum > 1)
        {
            PlayerParent.SetActive(true);
            joinMessage.SetActive(false);
            Joined = true;
            gameManager.numPlayers += 1;
        }
    }


    // start game
    public void StartGame()
    {
        // check if enough players
        if (gameManager.numPlayers > 1)
        {
            SceneManager.LoadScene(1);
            gameManager.PlayerInstantiate();
        }
    }

}
