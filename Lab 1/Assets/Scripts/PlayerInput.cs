using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PlayerController))]
public class PlayerInput : MonoBehaviour
{
    // player
    PlayerController player;


    private void Start() 
    {
        // set player
        player = gameObject.GetComponent<PlayerController>();  
    }

    // handle shooting
    private void Update() 
    {
        float xLeft = Input.GetAxisRaw("Horizontal_" + player.playerNum);
        float zLeft = -Input.GetAxisRaw("Vertical_" + player.playerNum);

        if (Input.GetButtonDown("Fire_" + player.playerNum))
        {
            player.Shoot();
        }

        if (zLeft < 0)
        {
            player.MoveDown();
        }
        else if (zLeft > 0)
        {
            player.MoveUp();
        }

        if (xLeft < 0)
        {
            player.MoveBack();
        }
        else if (xLeft > 0)
        {
            player.MoveFoward();
        }
    }

    // handle movement
    private void FixedUpdate() 
    {
        // get input from controller left stick
        //float xLeft = Input.GetAxisRaw("Horizontal_" + player.playerNum);
        //float zLeft = -Input.GetAxisRaw("Vertical_" + player.playerNum);
        //Debug.Log(xLeft);
        //Debug.Log(zLeft);


        /*if (zLeft > 0)
        {
            player.MoveDown();
        }
        else if (zLeft < 0)
        {
            player.MoveUp();
        }

        if (xLeft > 0)
        {
            player.MoveBack();
        }
        else if (xLeft < 0)
        {
            player.MoveFoward();
        }*/
    }
}
