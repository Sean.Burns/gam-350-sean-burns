using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    // variables
    new Rigidbody rigidbody;
    [SerializeField] GameObject bulletSpawnLocation;
    [SerializeField] GameObject bulletPrefab;
    public int playerNum;

    // Start is called before the first frame update
    void Start()
    {
        rigidbody = gameObject.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    // collision with water = lose
    private void OnCollisionEnter(Collision other) 
    {
        if (other.gameObject.tag == "water")
        {
            Destroy(gameObject);
        }   
    }

    // movement
    public void MoveFoward()
    {
        gameObject.transform.eulerAngles = new Vector3(0, 0, 0);
        rigidbody.AddForce( new Vector3(-10, 0, 0) );
        bulletPrefab.GetComponent<Bullet>().direction = 2;
    }
    public void MoveBack()
    {
        gameObject.transform.eulerAngles = new Vector3(0, 180, 0);
        rigidbody.AddForce( new Vector3(10, 0, 0) );
        bulletPrefab.GetComponent<Bullet>().direction = 4;
    }
    public void MoveUp()
    {
        gameObject.transform.eulerAngles = new Vector3(0, 90, 0);
        rigidbody.AddForce( new Vector3(0, 0, 10) );
        bulletPrefab.GetComponent<Bullet>().direction = 1;
    }
    public void MoveDown()
    {
        gameObject.transform.eulerAngles = new Vector3(0, -90, 0);
        rigidbody.AddForce( new Vector3(0, 0, -10) );
        bulletPrefab.GetComponent<Bullet>().direction = 3;
    }

    // shooting
    public void Shoot()
    {
        Instantiate(bulletPrefab, bulletSpawnLocation.transform.position, gameObject.transform.rotation);
    }
}
