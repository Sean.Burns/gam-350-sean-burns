using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorChange : MonoBehaviour
{

    UCNetwork network;
    // color change function
    public void ChangeColor(int id, int r, int g, int b)
    {
        //gameObject.GetComponent<MeshRenderer>().material.color = color;
        network.GetGameObject(id).GetComponent<MeshRenderer>().material.color = new Color(r, g, b);
    }

    // on cick call rpc
    /*private void OnMouseDown() 
    {
        Debug.Log("MouseDown");
        gameObject.GetComponent<NetworkSync>().CallRPC("ChangeColor", UCNetwork.MessageReceiver.AllClients, 1, Color.blue);
    }*/

    // on cick call rpc
    private void Update()
    {
 
        if (Input.GetButtonDown("Fire1"))
        {
            Debug.Log("Shoot");
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, Mathf.Infinity))
            {
                if (hit.collider != null) // if equal to player(clone)
                {
                    Debug.Log("Hit! " + hit.transform.name);
                    hit.transform.gameObject.GetComponent<NetworkSync>().CallRPC("ChangeColor", UCNetwork.MessageReceiver.AllClients, hit.transform.gameObject.GetComponent<NetworkSync>().GetId(), hit.transform.gameObject.GetComponent<NetworkSync>().GetId(), 0, 0, 1);
                    //CallRPC("ChangeColor", UCNetwork.MessageReceiver.AllClients, 1, Color.blue);
                }
            }
            Debug.DrawRay(Camera.main.ScreenPointToRay(Input.mousePosition).origin, Camera.main.ScreenPointToRay(Input.mousePosition).direction, Color.blue);
        }

        //Debug.Log("MouseDown");
        //gameObject.GetComponent<NetworkSync>().CallRPC("ChangeColor", UCNetwork.MessageReceiver.AllClients, 1, Color.blue);
    }
}
