﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ExampleClient : MonoBehaviour
{
    public ClientNetwork clientNet;
    
    // Are we in the process of logging into a server
    private bool loginInProcess = false;

    public GameObject loginScreen;

    public GameObject myPlayer;

    float deltaTime = 0;


    // Initialize the client
    void Awake()
    {
        // Make sure we have a ClientNetwork to use
        if (clientNet == null)
        {
            clientNet = GetComponent<ClientNetwork>();
        }
        if (clientNet == null)
        {
            clientNet = (ClientNetwork)gameObject.AddComponent(typeof(ClientNetwork));
        }
    }

    private void Start()
    {
        //Instantiate(myPlayer);
    }

    // Start the process to login to a server
    public void ConnectToServer(string aServerAddress, int aPort)
    {
        // If we're not already logging in
        if (loginInProcess)
        {
            return;
        }
        loginInProcess = true;
        
        ClientNetwork.port = aPort;
        
        // Attempt to connect to the server with the given address and port
        clientNet.Connect(aServerAddress, ClientNetwork.port, "", "", "", 0);
    }

    // CALLBACK FUNCTIONS
    //  The following functions will be called by the ClientNetwork script while the game is running:

    // Network status changes
    void OnNetStatusNone()
    {
        Debug.Log("OnNetStatusNone called");
    }
    void OnNetStatusInitiatedConnect()
    {
        Debug.Log("OnNetStatusInitiatedConnect called");
    }
    void OnNetStatusReceivedInitiation()
    {
        Debug.Log("OnNetStatusReceivedInitiation called");
    }
    void OnNetStatusRespondedAwaitingApproval()
    {
        Debug.Log("OnNetStatusRespondedAwaitingApproval called");
    }
    void OnNetStatusRespondedConnect()
    {
        Debug.Log("OnNetStatusRespondedConnect called");
    }

    // When the client has finished connecting to the server
    void OnNetStatusConnected()
    {
        loginScreen.SetActive(false);
        Debug.Log("OnNetStatusConnected called");

        // Add this client to an area
        clientNet.AddToArea(1);

        //clientNet.CallRPC("DoNukeEffect", UCNetwork.MessageReceiver.ServerOnly, -1);
        //DoNukeEffect();

        Debug.Log("Send time: " + Time.realtimeSinceStartup);
        deltaTime = Time.realtimeSinceStartup;
    }

    /*public void DoNukeEffect()
    {
        Debug.Log("TESTING");
        Debug.Log("Receive time: " + Time.realtimeSinceStartup);
        deltaTime -= Time.realtimeSinceStartup;
        Debug.Log("PING: " + deltaTime);
    }*/

    // When the client starts disconnecting from the server
    void OnNetStatusDisconnecting()
    {
        Debug.Log("OnNetStatusDisconnecting called");

        if (myPlayer)
        {
            clientNet.Destroy(myPlayer.GetComponent<NetworkSync>().GetId());
            myPlayer = null;
        }
    }

    // When the client has finished disconnecting from the server
    void OnNetStatusDisconnected()
    {
        Debug.Log("OnNetStatusDisconnected called");

        // Restart the demo scene
        SceneManager.LoadScene("Client");
        
        loginInProcess = false;

        if (myPlayer)
        {
            clientNet.Destroy(myPlayer.GetComponent<NetworkSync>().GetId());
            myPlayer = null;
        }
    }

    // When the client has successfully told the server to be put into an area
    public void OnChangeArea()
    {
        Debug.Log("OnChangeArea called");

        // Tell the server we are ready
        myPlayer = clientNet.Instantiate("Player", Vector3.zero, Quaternion.identity);
        //Instantiate(gameObject, Vector3.zero, Quaternion.identity);

        myPlayer.GetComponent<NetworkSync>().AddToArea(1);
    }

    void OnDestroy()
    {
        if (myPlayer)
        {
            clientNet.Destroy(myPlayer.GetComponent<NetworkSync>().GetId());
        }
        if (clientNet.IsConnected())
        {
            clientNet.Disconnect("Peace out");
        }
    }

    // rpc to change colors of tagger
    public void SetTagger(int aNetId)
    {
        // find all players
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");

        // loop through all players 
        foreach (GameObject player in players)
        {
            // check if they are the new tagger
            if (player.GetComponent<NetworkSync>().networkId == aNetId)
            {
                // set to color of tagger
                player.GetComponent<Renderer>().material.color = Color.red;
            }
            else
            {
                // set to color of non tagger
                player.GetComponent<Renderer>().material.color = Color.white;
            }
        }
    }

    // rpc to delete a disconnected player
    public void DeleteDisconnect(long aNetId)
    {
        // find all players
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");

        // loop through all players
        foreach (GameObject player in players)
        {
            // check if player is the disconnecting player
            if (player.GetComponent<NetworkSync>().networkId == aNetId)
            {
                // destroy gameobject
                Destroy(player);
            }
        }
    }


}


