﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using LitJson;
using System.Linq;

public class ExampleServer : MonoBehaviour
{
    public ServerNetwork serverNet;

    public int portNumber = 603;
    int numPlayers = 0;

    public float tagDistance = 1f;

    public GameObject playerObject;

    // Represents a player
    public class _Player
    {
        public bool isIt = false;
        public int playerId;
        public long clientId;
        public Vector3 pos;
        public GameObject playerObject = null;
    }

    // tagger info
    public _Player tagger;
    [SerializeField] long taggerID;

    // other vars
    bool switching = false;
    long lastClientId;

    // all active players
    Dictionary<long, _Player> players = new Dictionary<long, _Player>();
    
    // Initialize the server
    void Awake()
    {
        // Initialization of the server network
        ServerNetwork.port = portNumber;
        if (serverNet == null)
        {
            serverNet = GetComponent<ServerNetwork>();
        }
        if (serverNet == null)
        {
            serverNet = (ServerNetwork)gameObject.AddComponent(typeof(ServerNetwork));
            Debug.Log("ServerNetwork component added.");
        }

        //serverNet.EnableLogging("rpcLog.txt");
    }

    // CALLBACK FUNCTIONS
    //  The following functions will be called by the ServerNetwork script while the game is running:

    // A client has just requested to connect to the server
    void ConnectionRequest(ServerNetwork.ConnectionRequestInfo data)
    {
        Debug.Log("Connection request from " + data.username);

        // Approve the connection
        serverNet.ConnectionApproved(data.id);
    }

    // A client has finished connecting to the server
    void OnClientConnected(long aClientId)
    {
        numPlayers += 1;

        // get connecting client id
        lastClientId = aClientId;
    }

    // A client has disconnected
    void OnClientDisconnected(long aClientId)
    {
        // find gameobject for client
        foreach (var item in players)
        {
            if (aClientId == players[item.Value.playerId].clientId)
            {
                // send rpc to destroy
                serverNet.CallRPC("DeleteDisconnect",  UCNetwork.MessageReceiver.AllClients, -1, players[item.Value.playerId].playerId);
                
                if (players[item.Value.playerId].isIt)
                {
                    // delete from library
                    players.Remove(item.Value.playerId);

                    // select a random client to be tagger
                    System.Random random = new System.Random();
                    int index = random.Next(players.Count);
                    long key = players.Keys.ElementAt(index);

                    SwitchTagger(players[key]);
                } 
            }
        }
    }

    // A network object has been instantiated by a client
    void OnInstantiateNetworkObject(ServerNetwork.IntantiateObjectData aObjectData)
    {
        // Get the network object information, store in a dictionary
        ServerNetwork.NetworkObject obj = serverNet.GetNetObjById(aObjectData.netObjId);

        // place player in dictionary
        players[aObjectData.netObjId] = new _Player(); 

        // check if first player to connect
        if (numPlayers == 1)
        {
            // set player to tagger
            players[aObjectData.netObjId].isIt = true; 
            tagger = players[aObjectData.netObjId]; 
            taggerID = aObjectData.netObjId;
        }
        else if (numPlayers > 1) 
        {
            // send rpc to set tagger
            serverNet.CallRPC("SetTagger", UCNetwork.MessageReceiver.AllClients, -1, tagger.playerId);
        }

        // set playwr id and client id
        players[aObjectData.netObjId].playerId = aObjectData.netObjId;
        players[aObjectData.netObjId].clientId = lastClientId;
    }

    // A client has been added to a new area
    void OnAddArea(ServerNetwork.AreaChangeInfo aInfo)
    {

    }

    // An object has been added to a new area
    void AddedObjectToArea(int aNetworkId)
    {

    }

    // Initialization data should be sent to a network object
    void InitializeNetworkObject(ServerNetwork.InitializationInfo aInfo)
    {
        
    }

    // A game object has been destroyed
    void OnDestroyNetworkObject(int aObjectId)
    {

    }

    private void Update()
    {
        Dictionary<int, ServerNetwork.NetworkObject> allObjs = serverNet.GetAllObjects();
    }

    private void FixedUpdate()
    {
        // loop through all players
        foreach(var item in players)
        {
            // check if tagger is touching another player
            if (tagger != players[item.Value.playerId] && Vector3.Distance(tagger.pos, players[item.Value.playerId].pos) <= tagDistance)
            {
                if (!switching)
                {
                    // switch taggers
                    SwitchTagger(players[item.Value.playerId]);
                    switching = true;
                }
            }
        }
        switching = false;
    }

    public void NetObjectUpdated(int aNetId)
    {
        //Debug.Log("Object has been updated:" + aNetId);

        ServerNetwork.NetworkObject obj = serverNet.GetNetObjById(aNetId);

        // set player position
        players[aNetId].pos = obj.position;


        //...
        long ownerClientId = serverNet.GetOwnerClientId(aNetId);
        serverNet.CallRPC("IsHunter", ownerClientId, aNetId, players[aNetId].isIt); // update if they're it
        //...
    }

    // switch who is tagger
    void SwitchTagger(_Player player)
    {
        tagger.isIt = false;
        tagger = player;
        player.isIt = true;

        taggerID = player.playerId;

        serverNet.CallRPC("SetTagger", UCNetwork.MessageReceiver.AllClients, -1, taggerID);
    }

    
}
