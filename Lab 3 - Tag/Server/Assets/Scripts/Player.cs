using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public bool isIt = false;
    public int netID;
    public LayerMask notTaggerLayer;

    // Start is called before the first frame update
    void Start()
    {
        notTaggerLayer = 6;
    }

    // Update is called once per frame
    void Update()
    {
        /*RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.right), out hit, Mathf.Infinity, notTaggerLayer))
        {
            Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
            Debug.Log("Hit");
            //GameObject.FindWithTag("Server").GetComponent<ExampleServer>().CollisionCheck(this.gameObject, hit.collider.gameObject);
        }*/
            
        /*if (gameObject.layer == 0)
        {
            RaycastHit hit;

            // Does the ray intersect any objects excluding the player layer
            if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.right), out hit, Mathf.Infinity, notTaggerLayer))
            {
                Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
                Debug.Log("Hit");
                GameObject.FindWithTag("Server").GetComponent<ExampleServer>().CollisionCheck(this.gameObject, hit.collider.gameObject);
            }
            /*else if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.right), out hit, .5f, notTaggerLayer))
            {
                //Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
                GameObject.FindWithTag("Server").GetComponent<ExampleServer>().CollisionCheck(this.gameObject, hit.collider.gameObject);
            }
            else if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.up), out hit, .5f, notTaggerLayer))
            {
                //Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
                GameObject.FindWithTag("Server").GetComponent<ExampleServer>().CollisionCheck(this.gameObject, hit.collider.gameObject);
            }
            else if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.down), out hit, .5f, notTaggerLayer))
            {
                //Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
                GameObject.FindWithTag("Server").GetComponent<ExampleServer>().CollisionCheck(this.gameObject, hit.collider.gameObject);
            }
            else
            {
                //Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * 1000, Color.white);
            }

            /*if (Physics.SphereCast(transform.position, 2, transform.right, out hit, 5, notTaggerLayer))
            {
                Debug.Log("hit");
                GameObject.FindWithTag("Server").GetComponent<ExampleServer>().CollisionCheck(this.gameObject, hit.collider.gameObject);
            }
        }*/
    }

    /*private void OnTriggerEnter(Collider other) 
    {
        if (isIt)
        {
            isIt = false;
            gameObject.GetComponent<Collider>().isTrigger = false;

            other.gameObject.GetComponent<Collider>().isTrigger = true;
            other.gameObject.GetComponent<Player>().isIt = true;
        }
        isIt = false;
        other.gameObject.GetComponent<Player>().isIt = true;

        other.gameObject.GetComponent<Collider>().isTrigger = true;
        gameObject.GetComponent<Collider>().isTrigger = false;
    }*/

    private void OnCollisionEnter(Collision other) 
    {

        /*if (isIt)
        {
            GameObject.FindWithTag("Server").GetComponent<ExampleServer>().CollisionCheck(this.gameObject, other.gameObject);
        }*/

        // if it transfer hunter to collision object
        //if (isIt || other.gameObject.GetComponent<Player>().isIt)
        //{
            /*if (!isIt)
            {
                //isIt = true;
                if(other.gameObject.GetComponent<Player>().isIt)
                {
                    isIt = true;
                    StartCoroutine(TaggerWait(gameObject));
                    //other.gameObject.GetComponent<Player>().isIt = false;
                }
            }
            if (isIt)
            {
                StartCoroutine(TaggerWait(other.gameObject));
                isIt = false;
                //other.gameObject.GetComponent<Player>().isIt = true;
            }
       // }*/
    }

    // function to wait one second to tag after becoming tagger
    IEnumerator TaggerWait(GameObject tagger)
    {

        yield return new WaitForSeconds(1);

        tagger.gameObject.GetComponent<Player>().isIt = true;
    }
}
