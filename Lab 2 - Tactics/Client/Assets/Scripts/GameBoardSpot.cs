using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameBoardSpot : MonoBehaviour
{
    // managers
    public GameBoardManager gameBoardManager;
    GameObject currentSpot;

    // function to call on click
    public void GetPosAndSpot()
    {

        // get gameobject
        currentSpot = this.gameObject;

        //loop through board
        foreach(Spot spot in gameBoardManager.gameBoard)
        {
            if (spot.spotObject == currentSpot)
            {
                gameBoardManager.turnSpot = spot;
                gameBoardManager.turnXPos = spot.spotXPos;
                gameBoardManager.turnYPos = spot.spotYPos;
            }
        }

        gameBoardManager.TakeAction();
    }
}
