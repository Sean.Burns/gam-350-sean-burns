using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

// class for a gameboard spot
public class Spot
{
    public int spotXPos;
    public int spotYPos;
    public bool enemyPlayer = false;
    public GameObject spotObject;
}

public class GameBoardManager : MonoBehaviour
{
    // variables

    public TacticsClient client;

    // move/attack position
    public int turnXPos;
    public int turnYPos;

    // spot pressed on
    public Spot turnSpot;

    // dictionary used when filling the gameboard list with data
    public Dictionary<int, Row> yRows = new Dictionary<int, Row>();

    // added y rows
    public List<GameObject> y0 = new List<GameObject>();
    public List<GameObject> y1 = new List<GameObject>();
    public List<GameObject> y2 = new List<GameObject>();
    public List<GameObject> y3 = new List<GameObject>();
    public List<GameObject> y4 = new List<GameObject>();
    public List<GameObject> y5 = new List<GameObject>();
    public List<GameObject> y6 = new List<GameObject>();
    public List<GameObject> y7 = new List<GameObject>();
    public List<GameObject> y8 = new List<GameObject>();
    public List<GameObject> y9 = new List<GameObject>();
    public List<GameObject> y10 = new List<GameObject>();
    public List<GameObject> y11 = new List<GameObject>();
    public List<GameObject> y12 = new List<GameObject>();
    public List<GameObject> y13 = new List<GameObject>();
    public List<GameObject> y14 = new List<GameObject>();
    public List<GameObject> y15 = new List<GameObject>();

    // class for y rows
    public class Row
    {
        public List<GameObject> places = new List<GameObject>();
    }

    // list that will be created 
    public List<Spot> gameBoard = new List<Spot>();


    // create the board
    public void CreateBoard(int x, int y)
    {
        SetYRows(y);

        for (int i = 0; i < y; i++) 
        {
            for (int k = 0; k < x; k++)
            {
                yRows[i].places[k].SetActive(true);

                // create a spot
                Spot newSpot = new Spot();

                // set x and y of spot
                newSpot.spotXPos = k; 
                newSpot.spotYPos = i;

                // set gameobject of spot 
                newSpot.spotObject = yRows[i].places[k];

                gameBoard.Add(newSpot);
            }
        }
    }

    // decide to move or attack
    public void TakeAction()
    {
        // check if my turn 
        if (client.myTurn)
        {
            // button pressed turn pos and spot filled

            if (turnSpot.enemyPlayer)
            {
                // attack
                client.RequestAttack(turnXPos, turnYPos);
            }
            else
            {
                // move
                client.RequestMove(turnXPos, turnYPos);
            }
        }
    }

    // update board
    public void UpdateBoard()
    {
        // loop through spots clear colors
        // place players red=enemy blue=ally yellow=you

        // clear spots
        foreach(Spot spots in gameBoard)
        {
            spots.spotObject.GetComponent<Image>().color = new Color(1, 1, 1);
            spots.enemyPlayer = false;
        }

        // place players
        foreach(var item in client.players) // KeyValuePair<string, string> entry in myDictionary
        {

            if (item.Value.health > 0)
            {
                for (int i = 0; i < gameBoard.Count; i++)
                {
                    if (gameBoard[i].spotXPos == item.Value.xPos)
                    {
                        if (gameBoard[i].spotYPos == item.Value.yPos)
                        {
                            if (item.Value.playerId == client.playerId)
                            {
                                gameBoard[i].spotObject.GetComponent<Image>().color = new Color(1, 1, 0);
                            }
                            else if (item.Value.team == client.players[client.playerId].team)
                            {
                                gameBoard[i].spotObject.GetComponent<Image>().color = new Color(0, 0, 1);
                            }
                            
                            if (item.Value.team != client.players[client.playerId].team)
                            {
                                gameBoard[i].spotObject.GetComponent<Image>().color = new Color(1, 0, 0);
                                gameBoard[i].enemyPlayer = true;
                            }
                        }
                    }
                }
            }
        }
    }

    // set y rows used in creation of board
    // fills dictionary
    public void SetYRows(int y)
    {
        for (int i = 0; i < 9; i ++)
        {
            // create row 
            Row newRow = new Row();

            // set row list and place in dictionary
            if (i == 0)
            {
                newRow.places = y0;
                yRows[0] = newRow;
            }
            else if (i == 1)
            {
                newRow.places = y1;
                yRows[1] = newRow;
            }
            else if (i == 2)
            {
                newRow.places = y2;
                yRows[2] = newRow;
            }
            else if (i == 3)
            {
                newRow.places = y3;
                yRows[3] = newRow;
            }
            else if (i == 4)
            {
                newRow.places = y4;
                yRows[4] = newRow;
            }
            else if (i == 5)
            {
                newRow.places = y5;
                yRows[5] = newRow;
            }
            else if (i == 6)
            {
                newRow.places = y6;
                yRows[6] = newRow;
            }
            else if (i == 7)
            {
                newRow.places = y7;
                yRows[7] = newRow;
            }
            else if (i == 8)
            {
                newRow.places = y8;
                yRows[8] = newRow;
            }
        }

        if (y > 8) // == 9
        {
            // create row
            Row newRow = new Row();

            // set row list
            newRow.places = y9;

            // place list in dictionary
            yRows[9] = newRow;
        }
        
        if (y > 9) // == 9 10
        {
            // create row
            Row newRow = new Row();

            // set row list
            newRow.places = y10;

            // place list in dictionary
            yRows[10] = newRow;
        }
        
        if (y > 10) // == 9 10 11
        {
            // create row
            Row newRow = new Row();

            // set row list
            newRow.places = y11;

            // place list in dictionary
            yRows[11] = newRow;
        }
        
        if (y > 11) // == 9 10 11 12
        {
            // create row
            Row newRow = new Row();

            // set row list
            newRow.places = y12;

            // place list in dictionary
            yRows[12] = newRow;
        }
        
        if (y > 12) // == 9 10 11 12 13
        {
            // create row
            Row newRow = new Row();

            // set row list
            newRow.places = y13;

            // place list in dictionary
            yRows[13] = newRow;
        }
        
        if (y > 13) // == 9 10 11 12 13 14
        {
            // create row
            Row newRow = new Row();

            // set row list
            newRow.places = y14;
            
            // place list in dictionary
            yRows[14] = newRow;
        }
        
        if (y > 14) // == 9 10 11 12 13 14 15
        {
            // create row
            Row newRow = new Row();

            // set row list
            newRow.places = y15;

            // place list in dictionary
            yRows[15] = newRow;
        }
    }

}
