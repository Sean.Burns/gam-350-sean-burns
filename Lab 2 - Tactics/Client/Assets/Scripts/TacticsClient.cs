﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class TacticsClient : MonoBehaviour
{
    public ClientNetwork clientNet;
    public GameBoardManager gameBoardManager;
    public bool gameStarted;

    // Are we in the process of logging into a server
    private bool loginInProcess = false;

    public GameObject loginScreen;
    public GameObject menuScreen;
    public TextMeshProUGUI playerList;
    public GameObject gameScreen;
    public TextMeshProUGUI chatLog;
    public TextMeshProUGUI displayMessage;
    public GameObject displayCanvas;
    public TextMeshProUGUI turnDisplay;

    int timeToStart;
    [HideInInspector] public bool myTurn;
    
    // Represents a player
    public class Player
    {
        public string name = "";
        public int playerId = 0;
        public int characterClass = 0;
        public int health = 0;
        public int team = 0;
        public int xPos;
        public int yPos;
        public bool ready = false;
        public GameObject playerObject = null;
    }

    // store player data for each client
    public Dictionary<int, Player> players = new Dictionary<int, Player>();

    // My player id
    public int playerId;

    // Use this for initialization
    void Awake()
    {
        // Make sure we have a ClientNetwork to use
        if (clientNet == null)
        {
            clientNet = GetComponent<ClientNetwork>();
        }
        if (clientNet == null)
        {
            clientNet = (ClientNetwork)gameObject.AddComponent(typeof(ClientNetwork));
        }

        // clear message
        ResetMessage();
        
    }

    // Start the process to login to a server
    public void ConnectToServer(string aServerAddress, int aPort)
    {
        if (loginInProcess)
        {
            return;
        }
        loginInProcess = true;

        ClientNetwork.port = aPort;
        clientNet.Connect(aServerAddress, ClientNetwork.port, "", "", "", 0);
    }

    void OnNetStatusConnected()
    {
        loginScreen.SetActive(false);
        menuScreen.SetActive(true);
        Debug.Log("OnNetStatusConnected called");

        clientNet.AddToArea(1);
    }

    void OnNetStatusDisconnected()
    {
        Debug.Log("OnNetStatusDisconnected called");
        //SceneManager.LoadScene("Client");

        loginInProcess = false;
    }

    void OnDestroy()
    {
        if (clientNet.IsConnected())
        {
            clientNet.Disconnect("Peace out");
        }
    }

    void UpdatePlayerList()
    {
        playerList.SetText(" ");

        foreach (var item in players)
        {
            playerList.text = (playerList.text + "\n - " + players[item.Value.playerId].name + " / Class: " + players[item.Value.playerId].characterClass + " / Team: " + players[item.Value.playerId].team + " / Ready: " + players[item.Value.playerId].ready);
        }
    }

    public void UpdateChatLog(string aMessage)
    {
        chatLog.SetText(chatLog.text + "\n " + aMessage);
    }

    public void UpdateSentChat(string aMessage, bool allPlayers)
    {
        if (allPlayers)
        {
            chatLog.SetText(chatLog.text + "\n You (All): " + aMessage);
        }
        else if (!allPlayers)
        {
            chatLog.SetText(chatLog.text + "\n You (Team): " + aMessage);
        }
    }

    // coroutine to start game
    IEnumerator StartGame()
    {
        displayCanvas.SetActive(true);

        // display time until game
        displayMessage.SetText(timeToStart + " Seconds until game start.");

        // wait time until start
        yield return new WaitForSeconds(timeToStart);

        // clear message 
        ResetMessage();

        // load game canvas
        menuScreen.SetActive(false);
        gameScreen.SetActive(true);
        gameStarted = true;
    }

    IEnumerator DisplayAttack(int aPlayerId, int x, int y)
    {
        displayCanvas.SetActive(true);

        // get player that was attacked
        int playerDamaged = new int();

        foreach(var item in players)
        {
            if (item.Value.xPos == x && item.Value.yPos == y)
            {
                playerDamaged = item.Value.playerId;
            }
        }

        // get damaged amount
        int dmg = 0;

        if (players[aPlayerId].characterClass == 1)
        {
            dmg = 30;
        }
        else
        {
            dmg = 10;
        }

        // display info
        displayMessage.SetText(players[aPlayerId].name + "  ->  " + players[playerDamaged].name + "\n " + players[playerDamaged].name + "  |  HP:  " + (players[playerDamaged].health - dmg));

        // wait time until resetting display
        yield return new WaitForSeconds(4);

        // reset message
        ResetMessage();
    }

    // reset display message
    public void ResetMessage()
    {
        displayMessage.SetText(" ");
        displayCanvas.SetActive(false);
    }

    //! LOGIN PHASE

    // RPC called by the server to tell this client what their player id is
    public void SetPlayerId(int aPlayerId)
    {
        playerId = aPlayerId;
        players[playerId] = new Player();
        players[playerId].playerId = playerId;
        Debug.Log("created player.");

        // add to player list
        UpdatePlayerList();
    }

    // RPC called by the server to tell this client which team they are on
    public void SetTeam(int team)
    {
        players[playerId].team = team;

        // update team on list for myself
        UpdatePlayerList();
    }

    public void NewPlayerConnected(int aPlayerId, int team)
    {
        players[aPlayerId] = new Player();
        players[aPlayerId].team = team;
        players[aPlayerId].playerId = aPlayerId;

        // add player to list
        UpdatePlayerList();
    }

    public void PlayerNameChanged(int aPlayerId, string name)
    {
        players[aPlayerId].name = name;

        // change name on list
        UpdatePlayerList();
    }

    public void PlayerIsReady(int aPlayerId, bool isReady)
    {
        players[aPlayerId].ready = isReady;

        // change ready on list
        UpdatePlayerList();
    }

    public void PlayerClassChanged(int aPlayerId, int type)
    {
        players[aPlayerId].characterClass = type;

        // change class on list
        UpdatePlayerList();
    }

    public void GameStart(int time)
    {
        timeToStart = time;
        StartCoroutine(StartGame());
    }

    // client to server 
    public void SetName(string name) 
    {
        clientNet.CallRPC("SetName", UCNetwork.MessageReceiver.ServerOnly, -1, name);
        UpdatePlayerList();
    }

    public void SetCharacterType(int type) 
    {
        clientNet.CallRPC("SetCharacterType", UCNetwork.MessageReceiver.ServerOnly, -1, type);
        UpdatePlayerList();
    }

    public void Ready(bool isReady) 
    {
        clientNet.CallRPC("Ready", UCNetwork.MessageReceiver.ServerOnly, -1, isReady);
        UpdatePlayerList();
    }


    //! GAME PHASE

    //Messages from the client to the server: (need to use somewhere)
    public void RequestMove(int x, int y) 
    {
        clientNet.CallRPC("RequestMove", UCNetwork.MessageReceiver.ServerOnly, -1, x, y);
    }

    public void RequestAttack(int x, int y) 
    {
        clientNet.CallRPC("RequestAttack", UCNetwork.MessageReceiver.ServerOnly, -1, x, y);
    }

    public void SendChat(string message) 
    {
        clientNet.CallRPC("SendChat", UCNetwork.MessageReceiver.ServerOnly, -1, message);
    }

    public void SendTeamChat(string message) 
    {
        clientNet.CallRPC("SendTeamChat", UCNetwork.MessageReceiver.ServerOnly, -1, message);
    }

    public void PassTurn() 
    {
        if (myTurn)
        {
            clientNet.CallRPC("PassTurn", UCNetwork.MessageReceiver.ServerOnly, -1);
        }
    }

    //Messages from the server to the client:
    public void SetMapSize(int x, int y) 
    {
        gameBoardManager.CreateBoard(x, y);
        gameBoardManager.UpdateBoard(); 
    }

    public void SetBlockedSpace(int x, int y) 
    {
        //! only sets one blocked space 
        for (int i = 0; i < gameBoardManager.gameBoard.Count; i++)
        {
            if (gameBoardManager.gameBoard[i].spotXPos == x)
            {
                if (gameBoardManager.gameBoard[i].spotYPos == y)
                {
                    gameBoardManager.gameBoard[i].spotObject.SetActive(false);
                }
            }
        }
    }

    public void SetPlayerPosition(int aPlayerId, int x, int y) 
    {
        // update player pos
        players[aPlayerId].xPos = x;
        players[aPlayerId].yPos = y;

        // update game board
        gameBoardManager.UpdateBoard();
    }

    public void StartTurn(int aPlayerId) 
    {
        // update board after move/attack
        gameBoardManager.UpdateBoard();

        // reset turn display 
        turnDisplay.SetText("Turn: ");

        // if your turn display you
        if (aPlayerId == playerId)
        {
            turnDisplay.SetText(turnDisplay.text + "(" + aPlayerId + ") " + " YOU " + " (Team: " + players[aPlayerId].team + ")");

            // set my turn to true
            myTurn = true;
        }
        else // display whos turn it is
        {
            turnDisplay.SetText(turnDisplay.text + "(" + aPlayerId + ") " + players[aPlayerId].name + " (Team: " + players[aPlayerId].team + ")");

            // set my turn to false
            myTurn = false;
        }

    }

    public void AttackMade(int aPlayerId, int x, int y) 
    {
        StartCoroutine(DisplayAttack(aPlayerId, x, y));

        gameBoardManager.UpdateBoard();
    }

    public void DisplayChatMessage(string message) 
    {
        UpdateChatLog(message);
    }

    public void UpdateHealth(int aPlayerId, int newHealth) 
    {
        players[aPlayerId].health = newHealth;
    }
}


