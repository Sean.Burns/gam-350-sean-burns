using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LoginPhase : MonoBehaviour
{
    // variables
    public string name;
    public TMP_InputField inputField;

    public TacticsClient client;

    // call set name rpc
    public void SetName()
    {
        name = inputField.text;
        client.players[client.playerId].name = name;
        client.SetName(name);
    }

    // change class
    public void SetWarrior()
    {
        client.players[client.playerId].characterClass = 1;;
        client.SetCharacterType(1);
    }

    public void SetRogue()
    {
        client.players[client.playerId].characterClass = 2;
        client.SetCharacterType(2);
    }

    public void SetWizard()
    {
        client.players[client.playerId].characterClass = 3;
        client.SetCharacterType(3);
    }

    // set ready
    public void SetReady()
    {
        if (client.players[client.playerId].ready)
        {
            client.players[client.playerId].ready = false;
            client.Ready(client.players[client.playerId].ready);
        }
        else if (!client.players[client.playerId].ready)
        {
            client.players[client.playerId].ready = true;
            client.Ready(client.players[client.playerId].ready);
        }
    }
}
