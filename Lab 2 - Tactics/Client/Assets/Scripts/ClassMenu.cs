using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ClassMenu : MonoBehaviour
{
    public TacticsClient client;
    public TextMeshProUGUI classText;
    public TextMeshProUGUI hpText;
    public GameObject classMenu;


    public void UpdateMenu()
    {
        // check what class and set text 
        if (client.players[client.playerId].characterClass == 1)
        {
            classText.SetText("Your Class: Warrior"); 
        }
        else if (client.players[client.playerId].characterClass == 2)
        {
            classText.SetText("Your Class: Rogue"); 
        }
        else if (client.players[client.playerId].characterClass == 3)
        {
            classText.SetText("Your Class: Wizard"); 
        }

        // update hp
        hpText.SetText("Your HP    : " + client.players[client.playerId].health);
    }


    // input handling
    private void Update()
    {
        if (client.gameStarted)
        {
            if (Input.GetKey(KeyCode.Tab))
            {
                UpdateMenu();
                classMenu.SetActive(true);
            }
            else
            {
                classMenu.SetActive(false);
            }
        }
    }
}
