using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GamePhase : MonoBehaviour
{
    // variables
    public TextMeshProUGUI chatLog;
    public string message;
    public TMP_InputField messageInput;

    public TacticsClient client;

    // call send message rpc
    public void SendMessage()
    {
        message = messageInput.text;
        client.SendChat(message);
        //client.UpdateSentChat(message, true);
    }

    // call send message rpc to team
    public void SendTeamMessage()
    {
        message = messageInput.text;
        client.SendTeamChat(message);
        //client.UpdateSentChat(message, false);
    }
}
