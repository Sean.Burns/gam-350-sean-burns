﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

    public float speed = 5;
    public ExampleClient client;

    bool canMove = true;
    public bool mouseControls = false;

    [HideInInspector] public int player = 0; 

    // Update is called once per frame
    void Update()
    {
        if (GetComponent<NetworkSync>().owned)
        {
            if (!mouseControls)
            {
                if (Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0 && canMove)
                {
                    Vector3 movement = new Vector3(Input.GetAxis("Horizontal") * speed * Time.deltaTime, Input.GetAxis("Vertical") * speed * Time.deltaTime, 0);
                    transform.position += movement;
                }
            }
            else
            {
                gameObject.transform.position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                gameObject.transform.position = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, 4.75f);
            }
        }
    }

    // on collision with wall stop movement
    private void OnCollisionEnter(Collision other) 
    {
        //Debug.Log("Wall");
        
        if (other.gameObject.tag == "Wall")
        {
            canMove = false;
        }

        canMove = true;
    }
}
