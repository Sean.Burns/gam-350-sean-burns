using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Goal : MonoBehaviour
{
    //enemies goal
    public int playerGoal;

    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.tag == "Puck")
        {
            GameObject.FindWithTag("Client").GetComponent<ExampleClient>().ScoreGoal(playerGoal);
        }
    }
}
