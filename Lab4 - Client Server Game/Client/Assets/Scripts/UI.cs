using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UI : MonoBehaviour
{
    //score texts
    public TextMeshProUGUI player1ScoreTxt;
    public TextMeshProUGUI player2ScoreTxt;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //function to update score
    public void GoalScored(int player1Score, int player2Score)
    {
        player1ScoreTxt.SetText(player1Score + " ");
        player2ScoreTxt.SetText(player2Score + " ");
    }
}
