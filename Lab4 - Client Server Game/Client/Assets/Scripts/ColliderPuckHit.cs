using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderPuckHit : MonoBehaviour
{
    public int direction = 0; // 1 up, 2 right, 3 down, 4 left

    private void OnCollisionEnter(Collision other) {
        if (other.gameObject.tag == "Puck")
        {
            GameObject.FindWithTag("Client").GetComponent<ExampleClient>().HitPuck(direction, other.gameObject.transform.position);
        }
    }
}
