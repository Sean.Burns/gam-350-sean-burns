﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ExampleClient : MonoBehaviour
{
    public ClientNetwork clientNet;

    // Get the instance of the client
    static ExampleClient instance = null;
    
    // Are we in the process of logging into a server
    private bool loginInProcess = false;

    public GameObject loginScreen;

    public GameObject myPlayer;

    // Singleton support
    public static ExampleClient GetInstance()
    {
        if (instance == null)
        {
            Debug.LogError("ExampleClient is uninitialized");
            return null;
        }
        return instance;
    }

    // store player data for each client
    //public Dictionary<int, Player> players = new Dictionary<int, Player>();

    // bool for if player has been spawned on correct side
    bool isSpawned = false;

    // UI manager
    public UI uiManager;

    // Use this for initialization
    void Awake()
    {
        // Make sure we have a ClientNetwork to use
        if (clientNet == null)
        {
            clientNet = GetComponent<ClientNetwork>();
        }
        if (clientNet == null)
        {
            clientNet = (ClientNetwork)gameObject.AddComponent(typeof(ClientNetwork));
        }
    }
    
    // Start the process to login to a server
    public void ConnectToServer(string aServerAddress, int aPort)
    {
        if (loginInProcess)
        {
            return;
        }
        loginInProcess = true;
        
        ClientNetwork.port = aPort;
        clientNet.Connect(aServerAddress, ClientNetwork.port, "", "", "", 0);
    }

    // Update is called once per frame
    void Update()
    {
        /*
        timeToSend -= Time.deltaTime;
        if (timeToSend <= 0)
        {
            clientNet.CallRPC("RequestMove", UCNetwork.MessageReceiver.ServerOnly, -1, 1, 1, "x");
            clientNet.CallRPC("Blah", UCNetwork.MessageReceiver.ServerOnly, -1, 1, 1, "x");
            timeToSend = 10;
        }
        */
    }

    public void UpdateState(int x, int y, string player)
    {
        // Update the visuals for the game
    }

    public void RPCTest(int aInt)
    {
        Debug.Log("RPC Test has been called with " + aInt);
    }

    public void NewClientConnected(long aClientId, string aValue)
    {
        Debug.Log("RPC NewClientConnected has been called with " + aClientId + " " + aValue);
    }

    // Networking callbacks
    // These are all the callbacks from the ClientNetwork
    void OnNetStatusNone()
    {
        Debug.Log("OnNetStatusNone called");
    }
    void OnNetStatusInitiatedConnect()
    {
        Debug.Log("OnNetStatusInitiatedConnect called");
    }
    void OnNetStatusReceivedInitiation()
    {
        Debug.Log("OnNetStatusReceivedInitiation called");
    }
    void OnNetStatusRespondedAwaitingApproval()
    {
        Debug.Log("OnNetStatusRespondedAwaitingApproval called");
    }
    void OnNetStatusRespondedConnect()
    {
        Debug.Log("OnNetStatusRespondedConnect called");
    }
    void OnNetStatusConnected()
    {
        loginScreen.SetActive(false);
        Debug.Log("OnNetStatusConnected called");

        clientNet.AddToArea(1);
    }

    void OnNetStatusDisconnecting()
    {
        Debug.Log("OnNetStatusDisconnecting called");

        if (myPlayer)
        {
            clientNet.Destroy(myPlayer.GetComponent<NetworkSync>().GetId());
        }
    }
    void OnNetStatusDisconnected()
    {
        Debug.Log("OnNetStatusDisconnected called");
        SceneManager.LoadScene("Client");
        
        loginInProcess = false;

        if (myPlayer)
        {
            clientNet.Destroy(myPlayer.GetComponent<NetworkSync>().GetId());
        }
    }
    public void OnChangeArea()
    {
        Debug.Log("OnChangeArea called");

        // Tell the server we are ready
        myPlayer = clientNet.Instantiate("Player", Vector3.zero, Quaternion.identity);
        myPlayer.GetComponent<NetworkSync>().AddToArea(1);

        //? if transferring ownership
        //GameObject puck = clientNet.Instantiate("airhockeyPuck", new Vector3(0,1,4.5f), Quaternion.identity); //new Quaternion(0,0,0,0));
        //puck.transform.Rotate(-90.0f, 0.0f, 0.0f, Space.World);
    }

    // RPC Called by the server once it has finished sending all area initization data for a new area
    public void AreaInitialized()
    {
        Debug.Log("AreaInitialized called");
    }
    
    void OnDestroy()
    {
        if (myPlayer)
        {
            clientNet.Destroy(myPlayer.GetComponent<NetworkSync>().GetId());
        }
        if (clientNet.IsConnected())
        {
            clientNet.Disconnect("Peace out");
        }
    }


    //! RPC Functions

    // set player number
    public void SetPlayerIdx(int player, int netID)
    {
        // loop through and check
        if (GameObject.FindWithTag("Player").GetComponent<NetworkSync>().GetId() == netID)
        {
            GameObject.FindWithTag("Player").GetComponent<Player>().player = player;
        }
    }

    // set player position
    public void SetPlayerPosition()
    {
        if (myPlayer.GetComponent<Player>().player == 1)
        {
            myPlayer.transform.position = GameObject.FindWithTag("Spawn1").transform.position;
        }
        else
        {
            myPlayer.transform.position = GameObject.FindWithTag("Spawn2").transform.position;
        }
    }

    // set player color
    public void SetPlayerColor()//Vector3 color)
    {
        // set color
        Debug.Log("SetColor");

        // set one player only to blue
        /*if (myPlayer.GetComponent<Player>().player == 1)
        {
            //myPlayer.GetComponent<MeshRenderer>().material.color = Color.blue;
            GameObject.FindWithTag("Paddle").GetComponent<MeshRenderer>().material.color = Color.blue;
            GameObject.FindWithTag("Handle").GetComponent<MeshRenderer>().material.color = Color.blue;
        }
        else 
        {
            GameObject.FindWithTag("Paddle").GetComponent<MeshRenderer>().material.color = Color.red;
            GameObject.FindWithTag("Handle").GetComponent<MeshRenderer>().material.color = Color.red;
        }*/

        // set your client side player to blue
        GameObject.FindWithTag("Paddle").GetComponent<MeshRenderer>().material.color = Color.blue;
        GameObject.FindWithTag("Handle").GetComponent<MeshRenderer>().material.color = Color.blue;
    }

    // call rpc to sent to server that the puck was hit
    public void HitPuck(int direction, Vector3 puckPos)
    {
        // rpc call
        if (direction == 1)
        {
            clientNet.CallRPC("PuckHit", UCNetwork.MessageReceiver.ServerOnly, -1, 1, puckPos);
        }
        else if (direction == 2)
        {
            clientNet.CallRPC("PuckHit", UCNetwork.MessageReceiver.ServerOnly, -1, 2, puckPos);
        }
        else if (direction == 3)
        {
            clientNet.CallRPC("PuckHit", UCNetwork.MessageReceiver.ServerOnly, -1, 3, puckPos);
        }
        else if (direction == 4)
        {
            clientNet.CallRPC("PuckHit", UCNetwork.MessageReceiver.ServerOnly, -1, 4, puckPos);
        }
    }

    // call rpc to score goal
    public void ScoreGoal(int player)
    {
        if (player == 1)
        {
            clientNet.CallRPC("ScoreGoal", UCNetwork.MessageReceiver.ServerOnly, 1);
        }
        else if (player == 2)
        {
            clientNet.CallRPC("ScoreGoal", UCNetwork.MessageReceiver.ServerOnly, 2);
        }
    }

    // rpc to recieve about puck being hit
    public void PuckHitReturn(Vector3 puckPos, Vector3 force, float thrust)
    {
        //Debug.Log("Puckhit");
        GameObject.FindWithTag("Puck").transform.position = puckPos;
        GameObject.FindWithTag("Puck").GetComponent<Rigidbody>().AddForce(force * thrust);
    }

    // rpc to recieve when goal is scored
    public void UpdateScore(int player, int playerScore)
    {
        uiManager.GoalScored(player, playerScore);
    }

    // rpc to recieve when resetting the puck
    public void ResetPuck(bool gameStarting, int player)
    {
        //put puck in center
        GameObject.FindWithTag("Puck").transform.position = new Vector3 (0, 1, 4.5f);

        //reset force
        GameObject.FindWithTag("Puck").GetComponent<Rigidbody>().velocity = Vector3.zero;

        if (gameStarting)
        {
            // add random force
            GameObject.FindWithTag("Puck").GetComponent<Rigidbody>().AddForce(new Vector3 (Random.Range(0, 1), Random.Range(0, 1), 0) * 150);
        }
        else if (!gameStarting)
        {
            if (player == 1)
            {
                // send towards player 2
                GameObject.FindWithTag("Puck").GetComponent<Rigidbody>().AddForce(new Vector3 (-1, Random.Range(0, 1), 0) * 150);
            }
            else if (player == 2)
            {
                //send towards player 3
                GameObject.FindWithTag("Puck").GetComponent<Rigidbody>().AddForce(new Vector3 (1, Random.Range(0, 1), 0) * 150);
            }
        }
    }

    // rpc called by server when first player joins so only 1 set of goals exists
    //prevents double scoring issue
    public void AddGoals()
    {
        clientNet.Instantiate("Player1Goal", new Vector3(-23.32f,1.4f,4.41f), Quaternion.identity);
        clientNet.Instantiate("Player2Goal", new Vector3(23.32f,1.4f,4.41f), Quaternion.identity);
    }
}


