﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using LitJson;
using System.Linq;

public class ExampleServer : MonoBehaviour
{
    public ServerNetwork serverNet;

    public int portNumber = 603;
    int numPlayers = 0;

    //public float tagDistance = 1f;

    // game vars
    int player1Score = 0;
    int player2Score = 0;

    public GameObject playerObject;

    // Represents a player
    public class _Player
    {
        public int playerId;
        public long clientId;
        public Vector3 pos;
        public GameObject playerObject = null;
    }

    // other vars
    long lastClientId;
    bool goalsSpawned = false;

    // all active players
    Dictionary<long, _Player> players = new Dictionary<long, _Player>();
    
    // Initialize the server
    void Awake()
    {
        // Initialization of the server network
        ServerNetwork.port = portNumber;
        if (serverNet == null)
        {
            serverNet = GetComponent<ServerNetwork>();
        }
        if (serverNet == null)
        {
            serverNet = (ServerNetwork)gameObject.AddComponent(typeof(ServerNetwork));
            Debug.Log("ServerNetwork component added.");
        }

        //serverNet.EnableLogging("rpcLog.txt");
    }

    // CALLBACK FUNCTIONS
    //  The following functions will be called by the ServerNetwork script while the game is running:

    // A client has just requested to connect to the server
    void ConnectionRequest(ServerNetwork.ConnectionRequestInfo data)
    {
        Debug.Log("Connection request from " + data.username);

        // Approve the connection
        serverNet.ConnectionApproved(data.id);
    }

    // A client has finished connecting to the server
    void OnClientConnected(long aClientId)
    {
        numPlayers += 1;

        // get connecting client id
        lastClientId = aClientId;
    }

    // A client has disconnected
    void OnClientDisconnected(long aClientId)
    {
        // find gameobject for client
        /*foreach (var item in players)
        {
            if (aClientId == players[item.Value.playerId].clientId)
            {
                // send rpc to destroy
                serverNet.CallRPC("DeleteDisconnect",  UCNetwork.MessageReceiver.AllClients, -1, players[item.Value.playerId].playerId);
                
                if (players[item.Value.playerId].isIt)
                {
                    // delete from library
                    players.Remove(item.Value.playerId);

                    // select a random client to be tagger
                    System.Random random = new System.Random();
                    int index = random.Next(players.Count);
                    long key = players.Keys.ElementAt(index);

                    SwitchTagger(players[key]);
                } 
            }
        }*/
    }

    // A network object has been instantiated by a client
    void OnInstantiateNetworkObject(ServerNetwork.IntantiateObjectData aObjectData)
    {
        // Get the network object information, store in a dictionary
        ServerNetwork.NetworkObject obj = serverNet.GetNetObjById(aObjectData.netObjId);

        // place player in dictionary
        players[aObjectData.netObjId] = new _Player(); 

        // check if first player to connect
        if (numPlayers == 1)
        {
            // rpc to set player num
            serverNet.CallRPC("SetPlayerIdx", UCNetwork.MessageReceiver.AllClients, -1, 1, aObjectData.netObjId);

            // set player pos and color RPC
            serverNet.CallRPC("SetPlayerPosition", UCNetwork.MessageReceiver.AllClients, -1);
            serverNet.CallRPC("SetPlayerColor", UCNetwork.MessageReceiver.AllClients, -1);

            // add goals (prevernt 'double scoring issue')
            if (!goalsSpawned)
            {
                serverNet.CallRPC("AddGoals", UCNetwork.MessageReceiver.AllClients, -1);
                goalsSpawned = true;
            }
        }
        else if (numPlayers > 1) 
        {
            // rpc to set player num
            serverNet.CallRPC("SetPlayerIdx", UCNetwork.MessageReceiver.AllClients, -1, 2, aObjectData.netObjId);

            // set player pos and color RPC
            serverNet.CallRPC("SetPlayerPosition", UCNetwork.MessageReceiver.AllClients, -1);
            serverNet.CallRPC("SetPlayerColor", UCNetwork.MessageReceiver.AllClients, -1);

            // send rpc to start game
            StartGame();
        }

        // set playwr id and client id
        players[aObjectData.netObjId].playerId = aObjectData.netObjId;
        players[aObjectData.netObjId].clientId = lastClientId;
    }

    // A client has been added to a new area
    void OnAddArea(ServerNetwork.AreaChangeInfo aInfo)
    {

    }

    // An object has been added to a new area
    void AddedObjectToArea(int aNetworkId)
    {

    }

    // Initialization data should be sent to a network object
    void InitializeNetworkObject(ServerNetwork.InitializationInfo aInfo)
    {
        
    }

    // A game object has been destroyed
    void OnDestroyNetworkObject(int aObjectId)
    {

    }

    private void Update()
    {
        Dictionary<int, ServerNetwork.NetworkObject> allObjs = serverNet.GetAllObjects();
        checkScore();
    }

    private void FixedUpdate()
    {

    }

    /*public void NetObjectUpdated(int aNetId)
    {
        //Debug.Log("Object has been updated:" + aNetId);

        ServerNetwork.NetworkObject obj = serverNet.GetNetObjById(aNetId);

        // set player position
        players[aNetId].pos = obj.position;


        long ownerClientId = serverNet.GetOwnerClientId(aNetId);
    }*/

    // start game function
    private void StartGame()
    {
        // set player positions
        serverNet.CallRPC("SetPlayerPosition", UCNetwork.MessageReceiver.AllClients, -1);

        // set puck posittions
        serverNet.CallRPC("ResetPuck", UCNetwork.MessageReceiver.AllClients, -1,  true, 0);

        // update scores
        player1Score = 0;
        player2Score = 0;

        //send rpc to update ui
        serverNet.CallRPC("UpdateScore", UCNetwork.MessageReceiver.AllClients, -1, player1Score, player2Score);
    }

    // update function that checks scores 
    private void checkScore()
    {
        if (player1Score == 5)
        {
            // call start game function
            StartGame();

        }
        else if (player2Score == 5)
        {
            // call start game function
            StartGame();
        }
    }

    //! RPC Functions
    //? Client to Server

    // rpc when player hits puck
    public void PuckHit(int direction, Vector3 currPos)
    {
        // todo check if hit was valid

        float puckForceY = 1000f;
        float puckForceX = 1000f;

        // send rpc about new puck info
        if (direction == 1)
        {
            serverNet.CallRPC("PuckHitReturn", UCNetwork.MessageReceiver.AllClients, -1, currPos, Vector3.up, puckForceY);
        }
        else if (direction == 2)
        {
            serverNet.CallRPC("PuckHitReturn", UCNetwork.MessageReceiver.AllClients, -1, currPos, Vector3.right, puckForceX);
        }
        else if (direction == 3)
        {
            serverNet.CallRPC("PuckHitReturn", UCNetwork.MessageReceiver.AllClients, -1, currPos, Vector3.down, puckForceY);
        }
        else if (direction == 4)
        {
            serverNet.CallRPC("PuckHitReturn", UCNetwork.MessageReceiver.AllClients, -1, currPos, Vector3.left, puckForceX);
        }
    }

    // rpc when puck hits goal
    public void ScoreGoal(int player)
    {
        //todo check if goal was valid 

        // update scores 
        if (player == 1)
        {
            player1Score += 1; 

            // reset puck
            serverNet.CallRPC("ResetPuck", UCNetwork.MessageReceiver.AllClients, -1,  false, 1);
        }
        else if (player == 2)
        {
            player2Score += 1;

            // reset puck
            serverNet.CallRPC("ResetPuck", UCNetwork.MessageReceiver.AllClients, -1,  false, 2);
        }

        //send rpc to update ui
        serverNet.CallRPC("UpdateScore", UCNetwork.MessageReceiver.AllClients, -1, player1Score, player2Score);
    }
}
