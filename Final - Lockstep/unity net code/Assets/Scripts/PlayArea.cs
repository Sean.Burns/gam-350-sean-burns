using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayArea : MonoBehaviour
{
    private void OnTriggerExit(Collider other) 
    {
        if (other.gameObject.tag == "Puck")
        {
            GameObject.FindWithTag("PuckManager").GetComponent<PuckManager>().ResetPuck(true, 0);
        }
    }
}
