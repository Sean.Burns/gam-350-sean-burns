using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Goal : MonoBehaviour
{
    // what player should get score added
    public int player; 

    // call function to score goal
    private void OnTriggerEnter(Collider other) 
    {
        if (other.gameObject.tag == "Puck")
        {
            StartCoroutine(WaitScore());
        }
    }


    // wait to call score goal to allow for latency so client sees it go in goal
    private IEnumerator WaitScore()
    {
        yield return new WaitForSeconds(1);
        GameObject.FindWithTag("GameManager").GetComponent<GameManager>().ScoreGoal(player);
    }
}
