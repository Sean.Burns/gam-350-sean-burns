using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;

public class PuckManager : MonoBehaviour
{
    // variables
    public float puckForce = 700f;

    [SerializeField]
    private GameObject puck;

    [SerializeField]
    private Transform puckReset;

    [SerializeField]
    private float latencyMakeup;

    // Update is called once per frame
    void Update()
    {
        
    }

    // function to move puck after hitting it //! latency wait on hit
    public void HitPuck(Vector3 direction, GameObject player)
    {
        // set position to be the same for all clients
        puck.transform.position = puck.transform.position;

        // add force for hit
        puck.GetComponent<Rigidbody>().AddForce(direction * puckForce);
    }

    // reset puck to center (bool if game is starting, if not uses who scored to add force)
    public void ResetPuck(bool gameStarting, int player)
    {
        //put puck in center
        puck.transform.position = puckReset.position;

        //reset force
        puck.GetComponent<Rigidbody>().velocity = Vector3.zero;

        if (gameStarting)
        {
            // add random force
            puck.GetComponent<Rigidbody>().AddForce(new Vector3 (Random.Range(0, 1), Random.Range(0, 1), 0) * 150);
        }
        else if (!gameStarting)
        {
            if (player == 1)
            {
                // send towards player 2
                puck.GetComponent<Rigidbody>().AddForce(new Vector3 (-1, Random.Range(0, 1), 0) * 150);
            }
            else if (player == 2)
            {
                //send towards player 3
                puck.GetComponent<Rigidbody>().AddForce(new Vector3 (1, Random.Range(0, 1), 0) * 150);
            }
        }
    }
}
