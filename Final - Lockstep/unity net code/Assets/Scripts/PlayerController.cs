using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;

public class PlayerController : NetworkBehaviour
{

    [SerializeField]
    private float moveSpeed = 5f;

    [SerializeField]
    private GameObject hitColliders;

    private Vector3 lastPos;

    // Start is called before the first frame update
    void Start()
    {
        // set spawn point on start
        if (gameObject.GetComponent<NetworkObject>().IsOwnedByServer)
        {
            transform.position = GameObject.FindWithTag("P1Spawn").transform.position;
        }
        else
        {
            transform.position = GameObject.FindWithTag("P2Spawn").transform.position;

            // start game on second connect
            GameObject.FindWithTag("GameManager").GetComponent<GameManager>().StartGame();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
        // player movement
        if (gameObject.GetComponent<NetworkObject>().IsLocalPlayer)
        {
            if (Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0)
            {
                Vector3 movement = new Vector3(Input.GetAxis("Horizontal") * moveSpeed * Time.deltaTime, Input.GetAxis("Vertical") * moveSpeed * Time.deltaTime, 0);
                transform.position += movement;
            }
            else
            {
                transform.position += new Vector3(0,0,0);
                gameObject.GetComponent<Rigidbody>().velocity = new Vector3(0,0,0);
            }
        }

        hitColliders.transform.position = transform.position;
    }

    // fixed update
    private void FixedUpdate() 
    {
        // track last position
        lastPos = gameObject.transform.position;
    }

    // rpc call to set back to last known position
    //[ClientRpc]
    public void SetLastPosition()//ClientRpc()
    {
        // set player to last pos
        gameObject.transform.position = lastPos;
    }
}
