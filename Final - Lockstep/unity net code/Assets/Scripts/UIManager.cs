using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Unity.Netcode;
using Unity.Netcode.Transports.UNET;

public class UIManager : NetworkBehaviour
{
    // variables

    [SerializeField]
    private UNetTransport netTransport;

    [SerializeField]
    private NetworkManager networkManager;

    [SerializeField]
    private GameObject connectMenu;

    [SerializeField]
    private Button startHostButton;

    [SerializeField]
    private Button startClientButton;

    [SerializeField]
    private TextMeshProUGUI p1ScoreText;

    [SerializeField]
    private TextMeshProUGUI p2ScoreText;

    [SerializeField]
    private TextMeshProUGUI clientAddressInput;

    private void Awake()
    {
        Cursor.visible = true;
    }

    // starts hosting the server
    public void StartHostButton()
    {
        // start server
        if(NetworkManager.Singleton.StartHost())
        {
            //Debug.Log(GameObject.FindWithTag("NetworkManager").GetComponent<UNetTransport>().ConnectAddress);
            Debug.Log("Host Started... ");
        }
        else
        {
            Debug.Log("Host could not be started started... ");
        }

        // close connect menu
        CloseMenu();

    }

    // joins a host
    public void StartClientButton()
    {
        // get address
        //netTransport.ConnectAddress = clientAddressInput.text;

        // join host
        if(NetworkManager.Singleton.StartClient())
        {
             Debug.Log("Client Started... ");
        }
        else
        {
            Debug.Log("Client could not be started started... ");
        }

        // close connect menu
        CloseMenu();

    }

    // closes connect menu
    private void CloseMenu()
    {
        connectMenu.SetActive(false);
    }

    // update score
    [ClientRpc]
    public void UpdateScoreClientRpc(int p1Score, int p2Score)
    {
        p1ScoreText.SetText("" + p1Score);
        p2ScoreText.SetText("" + p2Score);
    }


}
