using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;

public class GameManager : NetworkBehaviour
{

    // variables
    private int p1Score = 0;
    private int p2Score = 0;

    public int scoreToWin = 5;

    public UIManager uiManager;

    public Transform p1Spawn;
    public Transform p2Spawn;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    // function to increase score, reset puck, and update ui
    public void ScoreGoal(int player)
    {
        // increase score 
        if (player == 1)
        {
            p1Score += 1;
        }
        else if (player == 2)
        {
            p2Score += 1;
        }

        // reset puck
        GameObject.FindWithTag("PuckManager").GetComponent<PuckManager>().ResetPuck(false, player);

        // update UI
        uiManager.UpdateScoreClientRpc(p1Score, p2Score);

        // check score
        CheckScore();
    }

    // checks if a player has the win condition score
    private void CheckScore()
    {
        if (p1Score == scoreToWin || p2Score == scoreToWin)
        {
            // call start game function
            StartGame();
        }
    }

    // reset player positions (1 to set player 1, 2 for 2, 0 for both)
    [ClientRpc]
    public void SetPlayerPositionsClientRpc(int player)
    {
        // get all players
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");

        for(int i = 0; i < players.Length; i++)
        {
            // check if player 1 or 2
            if (players[i].GetComponent<NetworkObject>().IsOwnedByServer && players[i].GetComponent<NetworkObject>().IsLocalPlayer)
            {
                // set to p1 transform
                if(player != 2)
                {
                    players[i].transform.position = p1Spawn.position;
                }
            }
            else if(!players[i].GetComponent<NetworkObject>().IsOwnedByServer && players[i].GetComponent<NetworkObject>().IsLocalPlayer)
            {
                // set to p2 transform
                if (player != 1)
                {
                    players[i].transform.position = p2Spawn.position;
                }
            }
        }
    }

    // call all functions to start the game
    public void StartGame()
    {
        // reset scores
        p1Score = 0;
        p2Score = 0;

        // update UI
        uiManager.UpdateScoreClientRpc(p1Score, p2Score);

        // set player position
        SetPlayerPositionsClientRpc(0);

        // reset puck
        GameObject.FindWithTag("PuckManager").GetComponent<PuckManager>().ResetPuck(true, 0);
    }
}
