using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;

public class ColliderPuckHit : MonoBehaviour
{
    public int direction = 0; // 1 up, 2 right, 3 down, 4 left

    public GameObject player;

    private void OnCollisionEnter(Collision other) 
    {
        
        if (other.gameObject.tag == "Puck")
        {
            // call puck hit function
            if (direction == 1)
            {
                GameObject.FindWithTag("PuckManager").GetComponent<PuckManager>().HitPuck(Vector3.up, player);
            }
            else if (direction == 2)
            {
                GameObject.FindWithTag("PuckManager").GetComponent<PuckManager>().HitPuck(Vector3.right, player);
            }
            else if (direction == 3)
            {
               GameObject.FindWithTag("PuckManager").GetComponent<PuckManager>().HitPuck(Vector3.down, player);
            }
            else if (direction == 4)
            {
                GameObject.FindWithTag("PuckManager").GetComponent<PuckManager>().HitPuck(Vector3.left, player);
            }
        }
    }
}
