using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    Vector3 targetPosition;
    float speed = 5.0f;

    void Start()
    {
        targetPosition = Input.mousePosition;
        Debug.Log("Target: " + targetPosition);
    }

    void Update()
    {
        if (GetComponent<NetworkSync>().owned)
        {
            Vector3 movementDirection = targetPosition - transform.position;
            movementDirection.Normalize();
            transform.position += movementDirection * speed * Time.deltaTime;
        }
    }
}
